<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel JWT React CRUD</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    <link href="'http://localhost/reacttask/public/css/app.css'" rel="stylesheet" />
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet"/>
    <!-- Styles -->

</head>
<body>
    <div id="index"></div>
    <script src="js/app.js"></script>
</body>
</html>
