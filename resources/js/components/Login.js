import React, {Component} from 'react';
import axios from 'axios'
import { useHistory } from "react-router-dom";


class Login extends Component {

    constructor(props) {
        super(props);
        this.state={
            email:'rislam252@gmail.com',
            password:'123123',
            url:'http://localhost/reacttask/public/api/auth/login',
            saveUrl:'http://localhost/reacttask/public/api/notes/save',
            token :''
        };

        this.emailChangeHandler= this.emailChangeHandler.bind(this);
        this.passwordChangeHandler= this.passwordChangeHandler.bind(this);
        this.login=this.login.bind(this);
        this.localStoredData = this.localStoredData.bind(this);

        this.localStoredData();
    }

    emailChangeHandler(e){
        var email = e.target.value;
        this.setState({
            email:email
        });
    }


    passwordChangeHandler(e){
        var password = e.target.value;

        this.setState({
            password:password
        })
    }

    login(e){
        e.preventDefault();
        const data = {
            email:this.state.email,
            password:this.state.password,
        };


        axios.post(this.state.url,data)
            .then(res=>{
                this.setState({token:res.data.access_token});
                localStorage.setItem('isLoggedIn',JSON.stringify(res.data.access_token));
                this.localStoredData();
            })
            .catch();


        var notes = localStorage.getItem('notes');
        var loggedIn = localStorage.getItem('isLoggedIn');
        if (notes){
            var parseData = JSON.parse(notes);

            const config ={
                headers : 'Barer '+loggedIn
            }
            for(var i =0;i<parseData.lenght;i++){
                const datas = {
                    title: parseData[i].title,
                    description: parseData[i].description,
                }
                axios.post("http://localhost/reacttask/public/api/notes/save",datas,config)
                    .then(res=>{
                        console.log(res);
                    })
                    .catch(err=>{
                        console.log(err);
                    })
            }
        }else{
            alert('failed')
        }
    }

    // Checking Local Storage data found or Not
    //IF found then it will be push in Live server

    localStoredData(){


    }
    componentDidMount() {
        this.localStoredData();
    }

    componentWillUpdate(){
        this.localStoredData();
    }





    render() {
        return (
            <div>
                <div className="row">
                    <div className="col-lg-2">
                    </div>
                    <div className="card-body  col-lg-8">
                        <div className="myForm">
                            <h4 className="bg-info text-center" style={{padding:"10px;color:#fff;"}}> ReactJs Login Form</h4>
                            <form onSubmit={this.login}>
                                <div className="form-group">
                                    <label >Email</label>
                                    <input onChange={this.emailChangeHandler} value={this.state.email} type="text" className="form-control"
                                           placeholder="rislam252@gmail.com" />
                                </div>
                                <div className="form-group">
                                    <label >Password</label>
                                    <input onChange={this.passwordChangeHandler} value={this.state.password}  type="text" className="form-control" id=""
                                           placeholder="Enter Password" />
                                </div>
                                <div className="form-group">
                                    <button type="submit" className="btn btn-info">Log-in</button>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
        );
    }
}

export default Login;
