import React, {Component} from 'react';

class Live extends Component {
    render() {
        return (
            <div>
                <div className="col-lg-6">
                    <div className="card-header">
                        <h4 className="card-title "> Data from Server </h4>
                    </div>

                    <div className="card-body">
                        <table className="table">
                            <thead className="thead-light">
                            <tr>
                                <th scope="col">SL</th>
                                <th scope="col">Title</th>
                                <th scope="col">Description</th>
                                <th scope="col">Manage</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <th scope="row">1</th>
                                <td>Mark</td>
                                <td>Otto</td>
                                <td>@mdo</td>
                            </tr>
                            <tr>
                                <th scope="row">2</th>
                                <td>Jacob</td>
                                <td>Thornton</td>
                                <td>@fat</td>
                            </tr>
                            <tr>
                                <th scope="row">3</th>
                                <td>Larry</td>
                                <td>the Bird</td>
                                <td>@twitter</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>

                </div>

            </div>
        );
    }
}

export default Live;
