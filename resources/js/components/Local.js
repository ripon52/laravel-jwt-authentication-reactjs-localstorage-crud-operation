import React, {Component} from 'react';

class Local extends Component {

    constructor(props){
        super(props);
        this.deleteNote = this.deleteNote.bind(this);
    }

    deleteNote(e){
        var id = e.target.id;
        var datas = JSON.parse(localStorage.getItem('notes'));
        datas.splice(id,1);
        localStorage.setItem('notes',JSON.stringify(datas));
        e.target.parentNode.parentElement.style.display="none";
    }



    render() {

        const locals= JSON.parse(localStorage.getItem('notes'));
        const todoItems =  locals.map((data,index)=>
                                <tr key={index}>
                                    <th >{index+1}</th>
                                    <td>{data.title}</td>
                                    <td>{data.description}</td>
                                    <td>
                                        <button type="button" id={index} onClick={this.deleteNote} className="btn btn-danger">Delete</button>
                                    </td>
                                </tr>
        )

        return (
            <div>
                <div className="col-lg-6">
                    <div className="card-header">
                        <h4 className="card-title"> Local stored Note </h4>
                    </div>
                    <div className="card-body">
                        <table className="table">
                            <thead className="thead-light">
                            <tr>
                                <th scope="col">SL</th>
                                <th scope="col">Title</th>
                                <th scope="col">Description</th>
                                <th scope="col">Manage</th>
                            </tr>
                            </thead>
                            <tbody>
                            {todoItems}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        );
    }
}

export default Local;
