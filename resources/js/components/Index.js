import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter as Router,Route,Link,Switch} from 'react-router-dom'

import 'bootstrap/dist/css/bootstrap.min.css';

import Header from "./Header";
import Login from "./Login";
import Main from "./Main";

export default class Index extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="container-fluid"  style={{padding:"50px 0 0 0"}}>
                <div className="container" >

                    <div className="jumbotron border border-success">
                        <h1>Laravel JWT with ReactJs</h1>
                    </div>

                    <Main/>

                </div>
            </div>

        );
    }
}

if (document.getElementById('index')) {
    ReactDOM.render( <Router>  <Index /> </Router>,document.getElementById('index'));
}
