import React, {Component} from 'react';
import {BrowserRouter as Router,Route,Switch,Link} from 'react-router-dom';
import { useHistory } from "react-router-dom";


import Login from "./Login";
import Index from "./Index";
import Note from "./Note";
import Local from "./Local";

class Main extends Component {

    constructor(props) {
        super(props);
        this.state={
            url:'http://localhost/reacttask/public/api/auth/login'
        };
        this.logOut = this.logOut.bind(this);
    }

    logOut(e){
        localStorage.removeItem('isLoggedIn');
        history.push("/");
    }

    render() {

        var isLoggedIn = localStorage.getItem('isLoggedIn');
        return (
            <Router>
                    <div>
                        <div className="row">
                            <div className="col-lg-4">
                                <Link to="/save-note" className="customButton" >New Note</Link>
                            </div>

                            {
                                isLoggedIn  ?
                                        <div className="col-lg-4">
                                            <button type="button" onClick={this.logOut} className="customButton" >Log out</button>
                                        </div>
                                :
                                    <div className="col-lg-4">
                                        <Link to="/login" className="customButton" >Login</Link>
                                    </div>
                            }

                            <div className="col-lg-4">
                                <Link to="/notes" className="customButton" >Browser Data's</Link>
                            </div>
                        </div>
                    </div>
                <Switch>
                    <Route exact path="/login" component={Login}/>
                    <Route exact path="/save-note" component={Note}/>
                    <Route exact path="/notes" component={Local}/>

                </Switch>
            </Router>
        );
    }
}

export default Main;
