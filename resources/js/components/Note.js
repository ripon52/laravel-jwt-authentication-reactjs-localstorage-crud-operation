import React, {Component} from 'react';
import axios from "axios";

class Note extends Component {


    constructor(props) {
        super(props);
        this.state={
            title:'',
            description:'',
            id:'',
            url:'http://localhost/reacttask/public/api/notes/save'
        };

        this.titleChangeHandler= this.titleChangeHandler.bind(this);
        this.descriptionChangeHandler= this.descriptionChangeHandler.bind(this);
        this.saveNote=this.saveNote.bind(this);
    }

    titleChangeHandler(e){
        var title = e.target.value;
        this.setState({
            title:title
        });
    }


    descriptionChangeHandler(e){
        var description = e.target.value;

        this.setState({
            description:description
        })
    }

    saveNote(e){
        e.preventDefault();

        const datas = [{
            title : this.state.title,
            description : this.state.description,
        }]

        var localDatas = localStorage.getItem('notes');

        if(!localDatas){
            localStorage.setItem('notes',JSON.stringify(datas));
        }else{
            var items = [];
            items = JSON.parse(localStorage.getItem('notes'));
            items.push(datas[0]);
            localStorage.setItem('notes',JSON.stringify(items));
        }

        this.setState({
            title:'',
            description:''
        });

        console.log(localStorage.getItem('notes'));
    }


    render() {

        return (
            <div>
                <div>
                    <div className="row">
                        <div className="col-lg-2">
                        </div>
                        <div className="card-body  col-lg-8">
                            <div className="myForm">
                                <h4 className="bg-info text-center" style={{padding:"10px;color:#fff;"}}> Data Save Inside Local Storage </h4>
                                <form onSubmit={this.saveNote}>
                                    <div className="form-group">
                                        <label >Title</label>
                                        <input onChange={this.titleChangeHandler} value={this.state.title} type="text" className="form-control"
                                               placeholder="Enter Title" />
                                    </div>
                                    <div className="form-group">
                                        <label >Description</label>
                                        <input onChange={this.descriptionChangeHandler} value={this.state.description}  type="text" className="form-control" id=""
                                               placeholder="Enter Password" />
                                    </div>
                                    <div className="form-group">
                                        <button type="submit" className="btn btn-info">Save inside Browser Storage</button>
                                    </div>
                                </form>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Note;
