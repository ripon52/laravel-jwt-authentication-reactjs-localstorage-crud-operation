<?php

use Illuminate\Http\Request;

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::get("/",function (){
    return view('welcome');
});

Route::group(['prefix' => 'auth'], function () {
    Route::post('login', 'AuthController@login');
    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::post('me', 'AuthController@me');
});

Route::middleware('JWT')->prefix('notes')->name("note.")->group(function(){
    Route::post("/save","NoteController@store")->name('save');
    Route::get("/list","NoteController@index")->name('view');
    Route::get("/{id}/edit",'NoteController@edit')->name('edit');
    Route::post("/{id}/update",'NoteController@update')->name('update');
    Route::post("/destroy",'NoteController@destroy')->name('destroy');
});
