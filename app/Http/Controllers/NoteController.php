<?php

namespace App\Http\Controllers;

use App\Note;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class NoteController extends Controller
{
    public  $user_id;
    public function __construct()
    {
        $this->user_id = Auth::id();
    }

    public function index(){
        return $this->view() ?? "No Data";
    }

    public function store(Request $request){
        $this->validate($request,[
            'title'=>'required'
        ]);
        $request['user_id'] = $this->user_id;
        Note::query()
            ->create($request->all());

        return response()->json([
            'message' => 'New note created',
            'notes' => $this->view()
        ]);
    }

    public function view(){
        return Note::query()->orderBy('title')->get();
    }

    public function edit($id){
        return Note::query()->findOrFail($id);
    }

    public function update(Request $request,$id){

        $request->validate([
            'title' => 'required',
            'description' => 'required'
        ]);

        $note = Note::query()->findOrFail($id);

        $note->update($request->all());

        return response()->json([
            'message' => 'Note data updated',
            'notes' => $this->view()
        ]);
    }

    public function destroy(Request $request){
        $note = Note::query()->findOrFail($request->id);
        $note->delete();
        return response()->json([
            'message' => 'Deleted ! Note data deleted',
            'notes' => $this->view()
        ]);
    }
}
